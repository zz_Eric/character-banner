# 字符画转换工具

>我们经常看见许多项目都有一个很靓的logo，是由标点符号组成的空心字母。该脚本旨在利用 linux 上自带的 figlet工具与lolcat工具，一键生成这些美丽的图案

## 使用说明

```
git clone https://gitee.com/xuthus5/character-banner.git
cd character-banner
sudo chmod +x main.sh
./main.sh
```

## 效果演示

![效果1](./preview/1.png)
![效果2](./preview/2.png)
![效果3](./preview/3.png)
![效果4](./preview/4.png)