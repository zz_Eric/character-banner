#!/bin/bash

Install() {
	type lolcat >&/dev/null
	lolcat=$?
	type figlet >&/dev/null
	figlet=$?
	if [ $lolcat -eq 0 ] && [ $figlet -eq 0 ]; then
		return
	fi

	echo "----------------------------------"
	echo "选择你的包管理工具："
	echo "1) yum"
	echo "2) dnf"
	echo "3) apt-get"
	echo "----------------------------------"
	read -p "输入安装版本编号(默认1):" tools
	if [ $lolcat -eq 1 ]; then
		if [ "${tools}" == "1" ]; then
			tools="yum"
		elif [ "${tools}" == "1" ]; then
			tools="dnf"
		else
			tools="apt-get"
		fi
		sudo "${tools}" install ruby
		wget https://github.com/busyloop/lolcat/archive/master.zip
		unzip master.zip
		cd lolcat-master/bin
		gem install lolcat
	fi

	if [ $figlet -eq 1 ]; then
		sudo dnf install figlet
	fi
}

Print() {
	figlet "${1}" | lolcat
}

Install

read -p "输入转换字符串：" string

if [ -z "${string}" ]; then
	string="H e l l o !"
fi

Print "${string}"
